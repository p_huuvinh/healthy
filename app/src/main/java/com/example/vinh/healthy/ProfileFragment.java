package com.example.vinh.healthy;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProfileFragment extends Fragment {

    private ListView profileList;
 //   private View profileView;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View profileView = inflater.inflate(R.layout.fragment_profile, container, false);
        profileList = (ListView) profileView.findViewById(R.id.profile_listview);
        ArrayList<String> list = new ArrayList<String>();
        list.add("My profile");
        list.add("My Device");
        list.add("Connect");
        list.add("Help");
        list.add("Sign out");

        ArrayAdapter<String> arrayAdapter;
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1 , list);
        profileList.setAdapter(arrayAdapter);
//        profileList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                String selectedItem = (String) adapterView.getItemAtPosition(i);
//                switch (i){
//                    case 2:
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),android.R.style.Theme_Black)
//
//                }
//            }
//        });
        return profileView;
    }

}

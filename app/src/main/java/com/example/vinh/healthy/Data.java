package com.example.vinh.healthy;

import java.util.Date;

public class Data{
    private int id;
    private int pulse;
    private int spo2;
    private int blood;
    private int temp;
    private String date;

    public Data(int id, int pulse, int spo2, int blood, int temp, String date) {
        this.id = id;
        this.pulse = pulse;
        this.spo2 = spo2;
        this.blood = blood;
        this.temp = temp;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public int getSpo2() {
        return spo2;
    }

    public void setSpo2(int spo2) {
        this.spo2 = spo2;
    }

    public int getBlood() {
        return blood;
    }

    public void setBlood(int blood) {
        this.blood = blood;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

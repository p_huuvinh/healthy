package com.example.vinh.healthy;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class HomeFragment extends Fragment implements OnChartValueSelectedListener {

//    public static final String	BOARD = Build.BOARD;
//    public static final String	BOOTLOADER = Build.BOOTLOADER;
//    public static final String	BRAND	= Build.BRAND;
//    public static final String	DEVICE	= Build.DEVICE;
//    public static final String	DISPLAY	= Build.DISPLAY;
//    public static final String	FINGERPRINT	= Build.FINGERPRINT;
//    public static final String	HARDWARE = Build.HARDWARE;
//    public static final String	HOST = Build.HOST;
//    public static final String	ID = Build.ID;
//    public static final String	MANUFACTURER = Build.MANUFACTURER;
//    public static final String	MODEL = Build.MODEL;
//    public static final String	PRODUCT = Build.PRODUCT;
//    public static final String	SERIAL = Build.SERIAL;
//    public static final String	TAGS = Build.TAGS;
//    public static final long	TIME = Build.TIME;
//    public static final String	TYPE = Build.TYPE;
//    public static final String	USER = Build.USER;

    private String mResMessage;
    private FloatingActionButton btn_connect;
    private TextView text_blood_prs_systolic;
    private TextView text_pulse;
    private TextView text_temp;
    private TextView text_spo2;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View homeView = inflater.inflate(R.layout.fragment_home, container, false);
        JSONArray jsonUserData = new JSONArray();
        try {

            jsonUserData = Constants.jsData.getJSONArray("index");
            Log.i("JSON: ", jsonUserData.getJSONObject(jsonUserData.length()-1).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        text_blood_prs_systolic = (TextView) homeView.findViewById(R.id.blood_prs_systolic);
        text_pulse = (TextView) homeView.findViewById(R.id.pulse);
        text_temp = (TextView) homeView.findViewById(R.id.temp);
        text_spo2 = (TextView) homeView.findViewById(R.id.spo2);
        try {
            text_blood_prs_systolic.setText(jsonUserData.getJSONObject(jsonUserData.length()-1)
                    .getString("Blood_pressure"));
            text_pulse.setText(jsonUserData.getJSONObject(jsonUserData.length()-1)
                    .getString("Pulse"));
            text_temp.setText(jsonUserData.getJSONObject(jsonUserData.length()-1)
                    .getString("Temp"));
            text_spo2.setText(jsonUserData.getJSONObject(jsonUserData.length()-1)
                    .getString("Oxygen"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btn_connect = homeView.findViewById(R.id.btn_connect);

        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forTest();
                postData();
                try {
                    Snackbar.make(view, Constants.jsData.getString("_id"), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                updateComponent();
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.detach(HomeFragment.this).attach(HomeFragment.this).commit();
            }
        });
        return homeView;
    }

    private void forTest(){
        Constants.data_blood = 123;
        Constants.data_pulse = 44;
        Constants.data_spo2 = 88;
        Constants.data_temp = 36;
    }

    private void updateComponent()
    {
        text_blood_prs_systolic.setText(String.valueOf(Constants.data_blood));
        text_temp.setText(String.valueOf(Constants.data_temp));
        text_pulse.setText(String.valueOf(Constants.data_pulse));
        text_spo2.setText(String.valueOf(Constants.data_spo2));

    }
    private void postData() {
        final String[] serverResponse = new String[1];
        final OkHttpClient client = new OkHttpClient();



        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.DATA_URL).newBuilder();
        urlBuilder.addQueryParameter("email", Constants.email);
        urlBuilder.addQueryParameter("password", Constants.password);
        urlBuilder.addQueryParameter("blood", Integer.toString(Constants.data_blood));
        urlBuilder.addQueryParameter("pulse", Integer.toString(Constants.data_pulse));
        urlBuilder.addQueryParameter("oxygen", Integer.toString(Constants.data_spo2));
        urlBuilder.addQueryParameter("temp", Integer.toString(Constants.data_temp));

        String url = urlBuilder.build().toString();

        final Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mResMessage = e.getMessage().toString();
                Log.w("failure Response", mResMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                mResMessage = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        Constants.jsData = new JSONObject(mResMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Home: mResMessage", mResMessage);
            }
        });

        if (mResMessage != null) {
            // Account exists, return true if the password matches.
            Log.i("Vinh: return",mResMessage);

        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    private void addData()
    {

    }
}

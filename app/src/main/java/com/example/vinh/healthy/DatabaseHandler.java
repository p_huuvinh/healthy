package com.example.vinh.healthy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.DESedeKeySpec;

class DatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "health";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME_DATA_DETAIL = "data_detail";
    private static final String DATA = "data";
    //table data
    private static final String USER_ID = "user_id";
    private static final String DETAIL_ID = "detail_id";
    //table detail data
    private static final String PULSE = "pulse";
    private static final String BLOOD_PRESSURE = "blod_pressure";
    private static final String SPO2 = "spo2";
    private static final String TEMP = "temp";
    private static final String DATE = "date";
    public DatabaseHandler(Context context, String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String create_table_data_detail = String.format("CREATE TABLE %s(%s " +
                        "INTEGER PRIMARY KEY, %s INT, %s INT, %s INT, %s INT, %s TEXT)",
                TABLE_NAME_DATA_DETAIL, DETAIL_ID, PULSE, BLOOD_PRESSURE,SPO2,TEMP,DATE);
        sqLiteDatabase.execSQL(create_table_data_detail);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String drop_students_table = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_DATA_DETAIL);
        sqLiteDatabase.execSQL(drop_students_table);
        onCreate(sqLiteDatabase);
    }
    public void addData(Data data)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(PULSE,data.getPulse());
        values.put(BLOOD_PRESSURE, data.getBlood());
        values.put(SPO2,data.getSpo2());
        values.put(TEMP,data.getTemp());
        values.put(DATE, data.getDate());
        db.insert(TABLE_NAME_DATA_DETAIL, null, values);
        db.close();
    }



    public Data getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME_DATA_DETAIL, null, DETAIL_ID + " = ?",
                new String[] { String.valueOf(id) },null, null, null);
        if(cursor != null)
            cursor.moveToFirst();
        Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3),
                cursor.getInt(4), cursor.getString(5));
        return data;
    }

    public List<Data> getAllData(){
        List<Data> dataList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME_DATA_DETAIL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                    cursor.getInt(3),cursor.getInt(4),cursor.getString(5));
            dataList.add(data);
            cursor.moveToNext();
        }
        return dataList;
    }

    public void updateStudent(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PULSE,data.getPulse());
        values.put(BLOOD_PRESSURE, data.getBlood());
        values.put(SPO2,data.getSpo2());
        values.put(TEMP,data.getTemp());
        values.put(DATE, data.getDate());

        db.update(TABLE_NAME_DATA_DETAIL, values, DETAIL_ID + " = ?",
                new String[] { String.valueOf(data.getId()) });
        db.close();
    }

    public void deleteStudent(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_DATA_DETAIL, DETAIL_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
}

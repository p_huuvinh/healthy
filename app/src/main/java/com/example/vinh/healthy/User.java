package com.example.vinh.healthy;

public class User {
    private String email;
    private String firstName;
    private String lastName;
    private String sessionId;

    public User(String email, String firstName, String lastName, String sessionId) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sessionId = sessionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}

package com.example.vinh.healthy;

import org.json.JSONObject;

import okhttp3.MediaType;

public class Constants {
    public static String email;
    public static JSONObject jsData;
    public static final String LOGIN_URL = "http://3.0.142.116:8888/login";
    public static final String REGISTER_URL = "http://3.0.142.116:8888/register";
    public static final String DATA_URL = "http://3.0.142.116:8888/data";
    public static String TOKEN = "token";
    public static final String EMAIL = "email";
    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json");
    public static String password;
    public static int data_pulse;
    public static int data_blood;
    public static int data_temp;
    public static int data_spo2;


}

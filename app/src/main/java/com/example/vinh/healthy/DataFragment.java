package com.example.vinh.healthy;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DataFragment extends Fragment implements OnChartValueSelectedListener {

    private static JSONArray jsonIndex;
    private CombinedChart mBloodChart;
    private CombinedChart mPulseChart;
    private CombinedChart mSPO2Chart;
    private CombinedChart mTempChart;
    //private JSONArray jsonIndex;
    public DataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        mBloodChart = (CombinedChart) view.findViewById(R.id.bloodPressure);
        mPulseChart = (CombinedChart) view.findViewById(R.id.pulseChart);
        mSPO2Chart = (CombinedChart) view.findViewById(R.id.spo2Chart);
        mTempChart = (CombinedChart) view.findViewById(R.id.tempChart);
        try {
            bloodPressureChart();
            pulseChart();
            spo2Chart();
            tempChart();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    private static DataSet dataBloodChart() {
        LineData d = new LineData();
        JSONObject j = new JSONObject();
        List<Integer> blood = null;

        try {
            jsonIndex = Constants.jsData.getJSONArray("index");
            blood = new ArrayList<>(jsonIndex.length());

            for(int i = jsonIndex.length() - 5; i<jsonIndex.length();i++){
                if(i<0)
                    continue;
                j= jsonIndex.getJSONObject(i);
                blood.add((int) j.getInt("Blood_pressure"));
                Log.i("Blood JSON:", j.toString());

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Entry> entries = new ArrayList<Entry>();

        for (int index = 0; index < blood.size(); index++) {
            entries.add(new Entry(index, blood.get(index)));
        }

        LineDataSet set = new LineDataSet(entries, "SYSTOLIC");
        set.setColor(Color.RED);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.BLUE);
        set.setCircleRadius(5f);
        set.setFillColor(Color.RED);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.BLUE);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return set;
    }

    private static DataSet dataPulseChart() {
        LineData d = new LineData();
        JSONObject j = new JSONObject();
        List<Integer> pulse = null;

        try {
            jsonIndex = Constants.jsData.getJSONArray("index");
            pulse = new ArrayList<>(jsonIndex.length());

            for(int i = jsonIndex.length() - 5; i<jsonIndex.length();i++){
                if(i<0)
                    continue;
                j= jsonIndex.getJSONObject(i);
                pulse.add((int) j.getInt("Pulse"));
                Log.i("Pulse JSON:", j.toString());

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Entry> entries = new ArrayList<Entry>();

        for (int index = 0; index < pulse.size(); index++) {
            entries.add(new Entry(index, pulse.get(index)));
        }

        LineDataSet set = new LineDataSet(entries, "Heart Rate");
        set.setColor(Color.RED);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.BLUE);
        set.setCircleRadius(5f);
        set.setFillColor(Color.RED);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.BLUE);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return set;
    }

    private static DataSet dataSPO2Chart() {
        LineData d = new LineData();
        JSONObject j = new JSONObject();
        List<Integer> pulse = null;

        try {
            jsonIndex = Constants.jsData.getJSONArray("index");
            pulse = new ArrayList<>(jsonIndex.length());

            for(int i = jsonIndex.length() - 5; i<jsonIndex.length();i++){
                if(i<0)
                    continue;
                j= jsonIndex.getJSONObject(i);
                pulse.add((int) j.getInt("Oxygen"));
                Log.i("Oxygen JSON:", j.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Entry> entries = new ArrayList<Entry>();

        for (int index = 0; index < pulse.size(); index++) {
            entries.add(new Entry(index, pulse.get(index)));
        }

        LineDataSet set = new LineDataSet(entries, "SPO2");
        set.setColor(Color.RED);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.BLUE);
        set.setCircleRadius(5f);
        set.setFillColor(Color.RED);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.BLUE);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return set;
    }

    private static DataSet dataTempChart() {
        LineData d = new LineData();
        JSONObject j = new JSONObject();
        List<Integer> temp = null;

        try {
            jsonIndex = Constants.jsData.getJSONArray("index");
            temp = new ArrayList<>(jsonIndex.length());

            for(int i = jsonIndex.length() - 5; i<jsonIndex.length();i++){
                if(i<0)
                    continue;
                j= jsonIndex.getJSONObject(i);
                temp.add((int) j.getInt("Temp"));
                Log.i("temp JSON:", j.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Entry> entries = new ArrayList<Entry>();

        for (int index = 0; index < temp.size(); index++) {
            entries.add(new Entry(index, temp.get(index)));
        }

        LineDataSet set = new LineDataSet(entries, "Body Temperature");
        set.setColor(Color.RED);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.BLUE);
        set.setCircleRadius(5f);
        set.setFillColor(Color.RED);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.BLUE);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return set;
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    private void bloodPressureChart() throws JSONException {
        // Inflate the layout for this fragment
        JSONObject j = new JSONObject();
        mBloodChart.getDescription().setEnabled(false);
        mBloodChart.setBackgroundColor(Color.TRANSPARENT);
        mBloodChart.setDrawGridBackground(false);
        mBloodChart.setDrawBarShadow(false);
        mBloodChart.setHighlightFullBarEnabled(false);
        mBloodChart.setOnChartValueSelectedListener( this);

        YAxis rightAxis = mBloodChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f);

        YAxis leftAxis = mBloodChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);

        List<String> xLabel = new ArrayList<>();
        jsonIndex = Constants.jsData.getJSONArray("index");
        for(int i = jsonIndex.length() - 5; i < jsonIndex.length();i++)
        {
            if(i<0)
                continue;
            j = jsonIndex.getJSONObject(i);
            xLabel = new ArrayList<>(jsonIndex.length());
            xLabel.add(j.getString("Date").split(",")[0]);
        }

        XAxis xAxis = mBloodChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        final List<String> finalXLabel = xLabel;
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return finalXLabel.get((int) value % finalXLabel.size());
            }
        });

        CombinedData data = new CombinedData();
        LineData lineDatas = new LineData();
        lineDatas.addDataSet((ILineDataSet) dataBloodChart());

        data.setData(lineDatas);

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);

        mBloodChart.setData(data);
        mBloodChart.invalidate();
    }

    private void pulseChart() throws JSONException {
        // Inflate the layout for this fragment
        JSONObject j = new JSONObject();
        mPulseChart.getDescription().setEnabled(false);
        mPulseChart.setBackgroundColor(Color.TRANSPARENT);
        mPulseChart.setDrawGridBackground(false);
        mPulseChart.setDrawBarShadow(true);
        mPulseChart.setHighlightFullBarEnabled(false);
        mPulseChart.setOnChartValueSelectedListener( this);
        YAxis rightAxis = mPulseChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f);

        YAxis leftAxis = mPulseChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);

        List<String> xLabel = new ArrayList<>();
        jsonIndex = Constants.jsData.getJSONArray("index");
        for(int i = jsonIndex.length() - 5; i < jsonIndex.length();i++)
        {
            if(i<0)
                continue;
            j = jsonIndex.getJSONObject(i);
            xLabel = new ArrayList<>(jsonIndex.length());
            xLabel.add(j.getString("Date").split(",")[0]);
        }

        XAxis xAxis = mPulseChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        final List<String> finalXLabel = xLabel;
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return finalXLabel.get((int) value % finalXLabel.size());
            }
        });

        CombinedData data = new CombinedData();
        LineData lineDatas = new LineData();
        lineDatas.addDataSet((ILineDataSet) dataPulseChart());

        data.setData(lineDatas);

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);

        mPulseChart.setData(data);
        mPulseChart.invalidate();
    }

    private void spo2Chart() throws JSONException {
        // Inflate the layout for this fragment
        JSONObject j = new JSONObject();
        mSPO2Chart.getDescription().setEnabled(false);
        mSPO2Chart.setBackgroundColor(Color.TRANSPARENT);
        mSPO2Chart.setDrawGridBackground(false);
        mSPO2Chart.setDrawBarShadow(false);
        mSPO2Chart.setHighlightFullBarEnabled(false);
        mSPO2Chart.setOnChartValueSelectedListener( this);

        YAxis rightAxis = mSPO2Chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f);

        YAxis leftAxis = mSPO2Chart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);

        List<String> xLabel = new ArrayList<>();
        jsonIndex = Constants.jsData.getJSONArray("index");
        for(int i = jsonIndex.length() - 5; i < jsonIndex.length();i++)
        {
            if(i<0)
                continue;
            j = jsonIndex.getJSONObject(i);
            xLabel = new ArrayList<>(jsonIndex.length());
            xLabel.add(j.getString("Date").split(",")[0]);
        }

        XAxis xAxis = mSPO2Chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        final List<String> finalXLabel = xLabel;
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return finalXLabel.get((int) value % finalXLabel.size());
            }
        });

        CombinedData data = new CombinedData();
        LineData lineDatas = new LineData();
        lineDatas.addDataSet((ILineDataSet) dataSPO2Chart());

        data.setData(lineDatas);

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);

        mSPO2Chart.setData(data);
        mSPO2Chart.invalidate();
    }

    private void tempChart() throws JSONException {
        // Inflate the layout for this fragment
        JSONObject j = new JSONObject();
        mTempChart.getDescription().setEnabled(false);
        mTempChart.setBackgroundColor(Color.TRANSPARENT);
        mTempChart.setDrawGridBackground(false);
        mTempChart.setDrawBarShadow(false);
        mTempChart.setHighlightFullBarEnabled(false);
        mTempChart.setOnChartValueSelectedListener( this);

        YAxis rightAxis = mTempChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f);

        YAxis leftAxis = mTempChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);

        List<String> xLabel = new ArrayList<>();
        jsonIndex = Constants.jsData.getJSONArray("index");
        for(int i = jsonIndex.length() - 5; i < jsonIndex.length();i++)
        {
            if(i<0)
                continue;
            if(i==0)
                i++;
            j = jsonIndex.getJSONObject(i);
            xLabel = new ArrayList<>(jsonIndex.length());
            xLabel.add(j.getString("Date").split(",")[0]);
        }

        XAxis xAxis = mTempChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        final List<String> finalXLabel = xLabel;
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return finalXLabel.get((int) value % finalXLabel.size());
            }
        });

        CombinedData data = new CombinedData();
        LineData lineDatas = new LineData();
        lineDatas.addDataSet((ILineDataSet) dataTempChart());

        data.setData(lineDatas);

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);

        mTempChart.setData(data);
        mTempChart.invalidate();
    }
}

package com.example.vinh.healthy;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class NotifyFragment extends Fragment {

   // private View notifyView;
    public NotifyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View notifyView = inflater.inflate(R.layout.fragment_notify,container,false);

        // Inflate the layout for this fragment
        return notifyView;
    }

}
